# usage:
# open gnuplot, and type:
# 	load "demo_3dplot.plt"

reset

load "magma.pal"

set hidden3d

set xlabel "longitude (deg)"
set ylabel "latitude (deg)"
set zlabel "height (km)"
set cblabel "height (km)"

set xyplane at 0
set grid x,y


#set view 0,0 # top view
#set view 3,350
set view 60,30 # the default side view

splot "demo3d_data.txt" u 4:5:3 lc palette pointtype 7 pointsize 1

# hold the left click on the figure and then move your mouse to make the 3D plot rotate.
