load "magma.pal"

set size ratio -1

set xlabel "longitude (deg)"
set ylabel "latitude (deg)"
set cblabel "cloud edge height (km)"


set xtics 1
set ytics 1
set grid front

## for a plot showing the scene and the satellite subtrack
set xr [294:302];
set yr [5:22];

# for a plot zooming on the scene
#set xr [300.38:301.32]
#set yr [13.56:14.15]


plot \
"world_10m.txt" u ( ($1+360) >= 294 && ($1+360) <= 302 ? ($1+360) : 1/0 ):($2 >= 5 && $2 <= 22 ? $2 : 1/0) w l lc -1 notitle, \
"../tutorial_outputs/12_00_10_A5_cloud_edge_height_longitude_latitude.csv" u 4:5:3 lc palette pt 7 ps .1 title "A5", "../angle_subsatellite_track/real_ALOS_A5_PAN.nc.txt" u 5:4 pt 5 lc rgb "#007400" title "subsat A5", \
"../tutorial_outputs/12_01_10_A4_cloud_edge_height_longitude_latitude.csv" u 4:5:3 lc palette pt 7 ps .1 title "A4", "../angle_subsatellite_track/real_ALOS_A4_PAN.nc.txt" u 5:4 pt 5 lc rgb "#21a354" title "subsat A4", \
"../tutorial_outputs/12_02_10_A3_cloud_edge_height_longitude_latitude.csv" u 4:5:3 lc palette pt 7 ps .1 title "A3", "../angle_subsatellite_track/real_ALOS_A3_PAN.nc.txt" u 5:4 pt 5 lc rgb "#74c476" title "subsat A3", \
"../tutorial_outputs/12_03_10_A2_cloud_edge_height_longitude_latitude.csv" u 4:5:3 lc palette pt 7 ps .1 title "A2", "../angle_subsatellite_track/real_ALOS_A2_PAN.nc.txt" u 5:4 pt 5 lc rgb "#a9e4a3" title "subsat A2", \
"../tutorial_outputs/12_04_10_A1_cloud_edge_height_longitude_latitude.csv" u 4:5:3 lc palette pt 7 ps .1 title "A1", "../angle_subsatellite_track/real_ALOS_A1_PAN.nc.txt" u 5:4 pt 5 lc rgb "#c0e8c9" title "subsat A1"


# pngcairo terminal:
#set term pngcairo size 2000,1000  ; set output "subsat_plot.png"; rep
