## just an example to be used e.g. with the export via to_dataframe().to_csv(), for the entire A3 xr.Dataset


load "magma.pal"
set view equal xy
set grid front

plot "../tutorial_outputs/12_02_10_A3.csv" u 5:4:3 lc palette pt 7 ps .5
