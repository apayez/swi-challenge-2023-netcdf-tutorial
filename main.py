# -*- coding: utf-8 -*-
# ---
# jupyter:
#   author: Alexandre Payez
#   jupytext:
#     formats: py:light,ipynb
#     notebook_metadata_filter: author,tags
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.13.5
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
#   tags:
#     event: SWI Challenge 2023
#     mission: ESA Harmony
# ---

# # SWI Challenge 2023 @ Groningen
# ## Dealing with netCDF files: a short tutorial to get started, using xarray

import numpy as np
import pandas as pd
import xarray as xr
import matplotlib.pyplot as plt

# when running the script outside of jupyter/IPython:
from IPython.display import DisplayObject, display

# ### Define views and timeslots

# +
views_DB = {
    "12_00_10": ["A5"],
    "12_01_10": ["A4"],
    "12_02_10": ["A3","B1"],
    "12_03_10": ["A2","B2"],
    "12_04_10": ["A1","B3"],
    "12_05_10": ["B4"],
    "12_06_10": ["B5"],
}

timeslots = list(views_DB.keys())
print(timeslots)
print()

print(f"There are {len(timeslots)} timeslots, and here is how they relate to the different views of Harmony A and B:")

for timeslot, views in views_DB.items():
    for view in views:
        print(timeslot, view)

# +
## use IPython magic to list all netCDF files in current working directory:
# # !ls | grep .nc
# -

# ## Open in xarray

# #### Open file

# +
# let us look at one of our netCDF files, as an example. Let's take the A3 one.

# sample_ds = xr.open_dataset("12_02_10_A3.nc", engine="netcdf4")
sample_ds = xr.load_dataset("12_02_10_A3.nc", engine="netcdf4")
# NB: - open_dataset opens lazily and turns the .nc file to read-only until the corresp. ds has been .closed().
#     - load_dataset loads all the .nc contents into memory closes the .nc file immediately after.

# info for that xr.Dataset:
display(sample_ds)
# -

# `xr.Datasets` replicate the internal structure of a netCDF file.
#
# They typically contain a collection of `xr.DataArrays`, and also store the corresponding metadata (e.g. `long_name`, `units`, `timeslot`, `view`).

# #### Basics

# +
# let's choose a sample layer among those
sample_layer = "cloud_edge_height"

# making a basic plot is easy:
sample_ds[sample_layer].plot()
plt.show()

# info for that xr.DataArray:
display(sample_ds[sample_layer])

# +
# the metadata can be accessed and used. 

# example: print out the min and max values of that layer with units:
print(f"min {sample_layer}:", float(sample_ds[sample_layer].min()), sample_ds[sample_layer].attrs["units"])
print(f"max {sample_layer}:", float(sample_ds[sample_layer].max()), sample_ds[sample_layer].attrs["units"])
# -

# ##### `.where()`

# Boolean indexing can be used in 1D; otherwise, one can use `.where()` to select data in 2D layers:

# e.g. plot the cloud_edge height, provided that it is larger than 2.3 km
sample_ds.cloud_edge_height.where(sample_ds.cloud_edge_height > 2.3).plot()
plt.show()

sample_ds.latitude.where(sample_ds.latitude > 13.85).plot()
plt.show()

# ##### numpy

# You can use `.values` to turn `xr.DataArrays` into numpy arrays:

sample_ds.cloud_edge_height.values # numpy.ndarray

sample_ds.cloud_edge_height.values.flatten() # 1D numpy.ndarray

# ## Simple plots, swap dims

# ### Simple example plots

plt.rc('font', size=12) # controls default text size for plots

for layer in sample_ds:
    print(layer)

# +
# let us loop and plot all the contents of our sample xr.Dataset:

timeslot = sample_ds.attrs["timeslot"]
view = sample_ds.attrs["view"]
title = f"{timeslot} {view}"

for layer in sample_ds:

    print(f"{layer}:")
    
    plt.figure(figsize=(15,8))
    sample_ds[layer].plot() # ix, iy
    plt.title(f"{title}")
    plt.show()
# -

# ### Swap dims

# +
# Same as above, but we first swap the pixel indices for the corresponding distances in km:

for layer in sample_ds:

    print(f"{layer}:")
    
    plt.figure(figsize=(15,8))
    
    ## plot after swapping nx and ny (pixels) with along-track and cross-track (distance in km)
    sample_ds.swap_dims({
    "ix": "x_DALES",
    "iy": "y_DALES",
    })[layer].plot() # along track, cross track
    plt.title(f"{title}")
    plt.show()
# -

# ## Plots: dealing with irregular latitude & longitude layers
# Let's say you want to plot against latitude, longitude, which are here both in the form of 2D arrays (irregular grid).

# ### `plt.contourf`

# +
# contourf uses 2D arrays directly (works both with xr.DataArrays and with np.arrays):

if False: # replace by True to execute the reminder of the cell

    plt.figure(figsize=(15,10))

    plt.contourf(sample_ds.longitude, 
                 sample_ds.latitude, 
                 sample_ds.cloud_edge_height, 
                 levels=20,                    # < increase for a smoother palette
                 cmap="magma"
                )

#     plt.xlim((300., 302))
#     plt.ylim((13.5, 14.2))
    plt.show()
# -

# So, there's no need to even turn them to numpy arrays via `.values`.

# ### (Slower) Turn the 2D xr.DataArrays into 1D numpy arrays, then do a scatter plot

# +
## xarray is not the best tool for everything.
## Switching to numpy at some point, e.g. before plotting, can be the sensible thing to do.

if False: # replace by True to execute the reminder of the cell

    lons = sample_ds.longitude.values.flatten()         # 1D numpy
    lats = sample_ds.latitude.values.flatten()          # 1D numpy
    cths = sample_ds.cloud_edge_height.values.flatten() # 1D numpy

    ## Then, you can e.g. simply do (nb: points plotted individually):
    plt.figure(figsize=(15,8))
    plt.scatter(lons, lats, c=cths, cmap='magma', s=0.1)
    ## Warning: do set s to a low value, otherwise visual glitches (large circles) appear due to the plotting process


    ## Don't do:
    # ## An example of the type of glitches you'd get with a large s (have a look at the left part of the image):
    # plt.scatter(lons, lats, c=cths, cmap='magma', s=200)
# -

# ### Further (all kinds of techniques exist to deal with irregular data with matplotlib)
# https://duckduckgo.com/?q=matplotlib+plot+irregular&t=ffab&ia=web gives a lot a good hits to address this general problem.

# ## Convert to pandas

# #### Turn a `xr.Dataset` into a `pd.DataFrame`, via `to_dataframe()`

sample_df = sample_ds.to_dataframe()
sample_df

# ## Create a new `xr.Dataset`

# +
## create an empty xr.Dataset
ds_out = xr.Dataset()

# let's fill it in with something
ds_out["height"] = sample_ds["cloud_edge_height"].copy(deep=True) # deep copy prevents modifying the original data

# we could do some processing here on ds_out and be sure that wouldn't affect sample_ds
ds_out["height"] = ds_out["height"].where(ds_out["height"] > 0.0675)

# have a look
display(ds_out)
ds_out.height.plot()
# -

# ## Getting the data out

# +
## now, write the output .nc files

# enable compression when writing to netCDF: https://stackoverflow.com/a/40818232
comp = {"zlib": True, "complevel": 9} # < very small file (just takes a bit of time to write)
encoding = {var: comp for var in ds_out.data_vars}

# write to file
ds_out.to_netcdf("tutorial_outputs/out.nc", encoding=encoding)
# -

# ### Export xarray data in csv format, via `to_dataframe()` and `to_csv()`

# export the entire xr.Dataset
if False: # replace by True to execute the reminder of the cell
    
    filename_out = f"tutorial_outputs/{sample_ds.timeslot}_{sample_ds.view}.csv"
    
    with open(filename_out, 'w') as file:
        file.write("#") # optional: comment out the header
        sample_ds.to_dataframe().to_csv(file, sep="\t", na_rep=np.nan)

# export a single xr.DataArray
if False: # replace by True to execute the reminder of the cell
    
    selected_array = "phi_view"
    filename_out = f"tutorial_outputs/{sample_ds.timeslot}_{sample_ds.view}_{selected_array}.csv"
    
    with open(filename_out, 'w') as file:
        file.write("#") # optional: comment out the header
        sample_ds[selected_array].to_dataframe().to_csv(file, sep="\t", na_rep=np.nan)

# export a selection of xr.DataArrays
if False: # replace by True to execute the reminder of the cell
    
    selected_arrays = ["cloud_edge_height", "longitude", "latitude",]
    filename_out = f"tutorial_outputs/{sample_ds.timeslot}_{sample_ds.view}_{'_'.join(selected_arrays)}.csv"
    
    with open(filename_out, 'w') as file:
        file.write("#") # optional: comment out the header
        sample_ds[selected_arrays].to_dataframe().to_csv(file, sep="\t", na_rep=np.nan)

# ### Realistic examples

if False:

    import glob

    # let's glob all .nc files corresponding to Harmony A
    for filename in sorted(glob.glob("*A*nc")):
        ds = xr.load_dataset(filename, engine="netcdf4")

        selected_arrays = ["cloud_edge_height", "longitude", "latitude",]
        filename_out = f"tutorial_outputs/{ds.timeslot}_{ds.view}_{'_'.join(selected_arrays)}.csv"

        with open(filename_out, 'w') as file:
            file.write("#") # optional: comment out the header
            ds[selected_arrays].to_dataframe().to_csv(file, sep="\t", na_rep=np.nan)

if False:

    selected_arrays = ["longitude", "latitude", "cloud_edge_height"]
    filename_out = f"tutorial_outputs/{sample_ds.timeslot}_{sample_ds.view}_{'_'.join(selected_arrays)}.csv"

    with open(filename_out, 'w') as file:
        file.write("#") # optional: comment out the header
        sample_ds[selected_arrays].to_dataframe().to_csv(file, sep="\t", na_rep=np.nan, index=False)

# ### (So (so) much slower) manual file dump

# +
# Replace False by True in the following line if you want to execute the reminder of the cell (slow execution)
if False:

    with open('tutorial_outputs/data_out.dat', 'w') as file:
        for ix in sample_ds.ix:
            for iy in sample_ds.iy:
                
                file.write(
                      f"{float(sample_ds.longitude.isel(ix=ix, iy=iy)):.4f}\t" \
                    + f"{float(sample_ds.latitude.isel(ix=ix, iy=iy)):.4f}\t"  \
                    + f"{float(sample_ds.cloud_edge_height.isel(ix=ix, iy=iy)):.4f}\n"
                )

# Will be pretty slow to write to the output text file...
# -
# ## Masking some edge
# There are some glitchy pixels in the top left of the images, we can mask them.

# +
## show original fig
# display(sample_ds)
plt.figure(figsize=(15,8))
sample_ds.cloud_edge_height.plot()
plt.show()
# plt.figure(figsize=(15,8))
# sample_ds.swap_dims({
#     "ix": "x_DALES",
#     "iy": "y_DALES",
# }).cloud_edge_height.plot()
# plt.show()

## show only ix pixels larger than some chosen threshold (beware, everything is still there if we stop using .where())
plt.figure(figsize=(15,8))
sample_ds.cloud_edge_height.where(sample_ds.ix >20).plot()
plt.show()

## mask for good
masked_ds = sample_ds.copy(deep=True)
masked_ds["cloud_edge_height"] = masked_ds.cloud_edge_height.where(sample_ds.ix > 20, drop=True)
display(masked_ds)

plt.figure(figsize=(15,8))
masked_ds.cloud_edge_height.plot()
plt.show()

plt.figure(figsize=(15,8))
masked_ds.swap_dims({
    "ix": "x_DALES",
    "iy": "y_DALES",
}).cloud_edge_height.plot() # NB: do swap dims before specifying the layer (otherwise, you might get weird results or a swapped fig)
plt.xlim(0,100)
plt.show()

# -

# ## Regridding example

# ### When a regular grid is needed (should not be the case here, really)
#     Matplotlib: gridding irregularly spaced data
#     --------------------------------------------
#     A commonly asked question on the matplotlib mailing lists is "how do I make a contour plot of my irregularly spaced data?". 
#     The answer is, first you interpolate it to a regular grid.
#
# source: https://scipy-cookbook.readthedocs.io/items/Matplotlib_Gridding_irregularly_spaced_data.html

if False: # replace by True to execute the reminder of the cell

    # Adapting ^ to the use of our own irregularly spaced data

    import numpy as np
    from scipy.interpolate import griddata
    import matplotlib.pyplot as plt

    plt.figure(figsize=(20,10))

    x = lons
    y = lats
    z = cths

    # define grid.
    xi = np.linspace(300.3,301.4,100)
    yi = np.linspace(13.5,14.2,100)

    # grid the data.
    # zi = griddata((x, y), z, (xi[None,:], yi[:,None]), method='cubic') ## beware that it can lead to *unphysical* values
    zi = griddata((x, y), z, (xi[None,:], yi[:,None]), method='linear')
    # zi = griddata((x, y), z, (xi[None,:], yi[:,None]), method='nearest') ## beware that it extrapolates for some reason

    # contour the gridded data, plotting dots at the randomly spaced data points.
    CS = plt.contour(xi,yi,zi,15,linewidths=0.5,colors='k')
    CS = plt.contourf(xi,yi,zi,15,cmap="magma")
    plt.colorbar() # draw colorbar

    plt.xlim(300.3, 301.4)
    plt.ylim(13.5,14.2)
    plt.title('regrid data test')
    plt.show()

if False: # replace by True to execute the reminder of the cell

    # Adapting ^ to the use of our own irregularly spaced data
    
    import numpy as np
    from scipy.interpolate import griddata
    import matplotlib.pyplot as plt
    plt.figure(figsize=(20,10))
    x = lons
    y = lats
    z = cths
    # define grid.
    xi = np.linspace(300.3,301.4,630) ## < here, I've increased to 630 (but one could put 1000 also of course)
    yi = np.linspace(13.5,14.2,1000)   ## < ______________________ 1000
    # grid the data.
    zi = griddata((x, y), z, (xi[None,:], yi[:,None]), method='linear')
    # contour the gridded data, plotting dots at the randomly spaced data points.
    # CS = plt.contour(xi,yi,zi,15,linewidths=0.5,colors='k')
    CS = plt.contourf(xi,yi,zi,100,cmap="magma")
    plt.colorbar() # draw colorbar

    ### we won't show the points because there are so many, we'd just see a big blue overlay
    # # plot data points.
    # plt.scatter(x,y,marker='o',c='b',s=5)


    plt.xlim(300.3, 301.4)
    plt.ylim(13.5,14.2)
    plt.title('regrid data test')
    plt.show()


