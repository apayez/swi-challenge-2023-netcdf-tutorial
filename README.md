# Harmony - netCDF Tutorial for the SWI Challenge 2023

Started in preparation for the SWI challenge (Groningen) — First version: January 20th 2023.

## Objective

We want to provide netCDF data with the appropriate layers for the challenge. Those are taken from SGM outputs for the DALES_highres scene (PAN), both from `lw_rad` (most layers) and from `model_los` (true cloud_edge_height) files.

We also illustrate a number of techniques for plotting, swapping dimensions, plotting irregular latitude/longitude grids, and regridding.

## Requirements

`pip install xarray numpy pandas matplotlib notebook jupytext netcdf4`

You might want to do that in a virtual environment (e.g. set up via `python3 -m venv`).
